/**
 * Imports
 */
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');





/**
 * Exports
 */
module.exports = {
    mode: process.env.NODE_ENV,
    entry: {
        common: './src/common.js',
        main: './src/index.js',
        competitions: './src/competitions.js',
        participate: './src/participate.js',
        terms: './src/terms.js',
        contact: './src/contact.js',
        faqs: './src/faqs.js'
    },
    output: {
        clean: true,
        filename: '_s/[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: process.env.NODE_ENV === 'production' ? './' : '/'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json']
    },
    devServer: {
        static: [path.join(__dirname, 'dist')],
        historyApiFallback: true,
        https: false,
        compress: true,
        port: 4000,
        liveReload: true,
        hot: false
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '_c/[name].[contenthash].css'
        }),
        new HtmlPlugin({
            minify: process.env.NODE_ENV === 'production',
            filename: 'index.html',
            template: 'src/pages/index.html',
            chunks: ['common', 'main']
        }),
        new HtmlPlugin({
            minify: process.env.NODE_ENV === 'production',
            filename: 'competitions.html',
            template: 'src/pages/competitions.html',
            chunks: ['common', 'competitions']
        }),        
        new HtmlPlugin({
            minify: process.env.NODE_ENV === 'production',
            filename: 'participate.html',
            template: 'src/pages/participate.html',
            chunks: ['common', 'participate']
        }),
        new HtmlPlugin({
            minify: process.env.NODE_ENV === 'production',
            filename: 'terms.html',
            template: 'src/pages/terms.html',
            chunks: ['common', 'terms']
        }),
        new HtmlPlugin({
            minify: process.env.NODE_ENV === 'production',
            filename: 'contact.html',
            template: 'src/pages/contact.html',
            chunks: ['common', 'contact']
        }),
        new HtmlPlugin({
            minify: process.env.NODE_ENV === 'production',
            filename: 'faqs.html',
            template: 'src/pages/faqs.html',
            chunks: ['common', 'faqs']
        }),
        new CopyPlugin({
            patterns: [{ from: 'images', to: '_i', context: 'src' }],
            options: { concurrency: 100 }
        })
    ],
    optimization: {
        minimize: process.env.NODE_ENV === 'production',
        minimizer: [
            new TerserPlugin({
                test: /\.js$/,
                exclude: /(node_modules)/,
                parallel: true
            }),
            new CssMinimizerPlugin()
        ]
    },
    module: {
        rules: [
            // JS files
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                ]
            },
            // SASS/CSS files
            {
                test: /\.scss$/,
                exclude: /(node_modules)/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            }
        ]
    }
};




