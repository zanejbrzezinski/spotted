### `npm install`

Creates `node_modules` folder. 

### `npm run build:prod`

Builds the app for production to the `dist` folder.<br>

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

