/**
 * Imports
 */

/**
 * Sidebar
 */
const Contact = (function() {

    /**
     * Variables
     */
    const select = document.querySelector('#contact-types');

    const opts = {
        entry: 'mailto:spottedsupport@jiosaavn.com?subject=Contest entry related query',
        shortlist: 'mailto:spottedsupport@jiosaavn.com?subject=Shortlisting related query',
        submission: 'mailto:spottedsupport@jiosaavn.com?subject=Song not getting submitted',
        other: 'mailto:spottedsupport@jiosaavn.com?subject=I have another issue'
    }

    /**
     * toggle sidebar
     */
    const openMailto = function(e) {
        window.gtag('event', 'click', {
            'screen_name': 'spotted',
            'screen_page_id': 'contact',
            'entity_name': e.target.valuie,
            'entity_type': 'select',
        })
        window.open(opts[e.target.value])
    };

    /**
     * Initialize
     */
    const init = function() {
        select.addEventListener('change', openMailto)
    };


    /**
     * Public methods
     */
    return { init };

})();





/**
 * Exports
 */
export default Contact;




