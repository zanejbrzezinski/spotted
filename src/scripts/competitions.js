/**
 * Imports
 */
 
import axios from 'axios';
import Carousel from './carousel'

/**
 * Competitions
 */
const Competitions = (function() {

    /**
     * Variables
     */
    const smallTile = document.querySelector('#tile-small');
    const largeTile = document.querySelector('#tile-large');
    const onGoing = document.querySelector('#ongoing');
    const upcoming = document.querySelector('#upcoming');
    const completed = document.querySelector('#completed');
    const bg1 = document.querySelector('#bg-1');
    const bg2 = document.querySelector('#bg-2');
    const bg3 = document.querySelector('#bg-3');
    const bgs = [bg1, bg2, bg3];

    let config = {
        headers: {
            'Access-Control-Allow-Headers': '*',
        }
    }

    /**
     * get Data
     */
    const getData = function(){
        const url = process.env.NODE_ENV === 'production' ? "https://www.jiosaavn.com/api.php?__call=spotted.competitionBoard&api_version=4&_format=json&_marker=0" : "https://staging.jiosaavn.com/api.php?__call=spotted.competitionBoard&api_version=4&_format=json&_marker=0"
        // const url = "https://staging.jiosaavn.com/api.php?__call=spotted.competitionBoard&api_version=4&_format=json&_marker=0"

        axios.get(url,config)
        .then((res) => {
            createMarkup(res.data);
        })
    }

    /**
     * create Tile Markup
     */
    const createMarkup = function(data){
        let markup = {};

        Object.keys(data).map((key) => {
            markup[key] = data[key].map((obj, i) => {
                if (key === 'onGoing') {
                    let elem = largeTile.cloneNode(true);
                    elem.removeAttribute('id');
                    elem.querySelector('.c-tile').classList.add(`c-tile--${i%6}`);
                    let bg = bgs[i%3].cloneNode();
                    bg.removeAttribute('id');
                    elem.querySelector('.c-tile').append(bg);
                    elem.querySelector('.c-tile__header').innerHTML = obj.language;
                    elem.querySelector('.c-tile__sub').innerHTML = obj.tag;

                    elem.querySelector('a').addEventListener('click', function(e){
                        window.gtag('event', 'click', {
                            'screen_name': 'spotted',
                            'screen_page_id': 'competitions',
                            'entity_name': 'participate_now',
                            'entity_type': 'link',
                            'entity_pos': obj.language + "_tile"
                        })
                    })

                    return elem;
                } else {
                    let elem = smallTile.cloneNode(true);
                    elem.removeAttribute('id');
                    elem.querySelector('.c-tile').classList.add(`c-tile--${i%6}`);
                    let bg = bgs[i%3].cloneNode();
                    bg.removeAttribute('id');
                    elem.querySelector('.c-tile').append(bg);
                    elem.querySelector('.c-tile__header').innerHTML = obj.language;
                    elem.querySelector('.c-tile__pill').innerHTML = obj.tag;
                    return elem;
                }
            })
        })
        appendMarkup(markup);
    }

    /**
     * Append Markup to DOM
     */
    const appendMarkup = function(markup){
        if (!markup.onGoing.length || !onGoing) {
            if (onGoing) {onGoing.classList.add('u-hidden');}
        } else {
            markup.onGoing.map((el) => {
                ongoing.querySelector('.c-loader').classList.add('u-hidden');
                onGoing.querySelector('.c-carousel__inner').append(el);
            })
        }
        if (!markup.upcoming.length || !upcoming) {
            if (upcoming) {upcoming.classList.add('u-hide');}
        } else {
            markup.upcoming.map((el) => {
                upcoming.querySelector('.c-loader').classList.add('u-hidden');
                upcoming.querySelector('.c-carousel__inner').append(el);
            })
        }
        if (!markup.completed.length || !completed) {
            if (completed) {completed.classList.add('u-hidden');}
        } else {
            markup.completed.map((el) => {
                completed.querySelector('.c-loader').classList.add('u-hidden');
                completed.querySelector('.c-carousel__inner').append(el);
            })
        }

        initCarousel();
    }

    const initCarousel = function(){
        if (onGoing) { new Carousel(true).init({id: 'ongoing'}) }
        if (upcoming) { new Carousel().init({id: 'upcoming'}) }
        if (completed) { new Carousel().init({id: 'completed'}) }
        
    }

    /**
     * Initialize
     */
    const init = function(small) {
        getData();
    };


    /**
     * Public methods
     */
    return { init };

})();





/**
 * Exports
 */
export default Competitions;




