
/**
* CONSTANTS
*/

export const PAGE_MAPPINGS = { '/' : 'landing',
        '/competitions' : 'competitions',
        '/competitions.html' : 'competitions',
        '/participate' : 'participate',
        '/participate.html' : 'participate',
        '/terms' : 'terms',
        '/terms.html' : 'terms',
        '/contact' : 'contact',
        '/contact.html' : 'contact',
        '/faqs' : 'faqs',
        '/faqs.html' : 'faqs',
    }


export const FAQS = {
    'All About JioSaavn Spotted': [
        {title: "What's JioSaavn Spotted?",
        copy: "<p>JioSaavn Spotted' is India’s first 360-degree artist discovery campaign that will be executed across all regions of India. The end goal of the campaign is to give a boost to India’s nascent independent music scene by spotting, grooming, and growing unheard, raw, and undiscovered talent, and showcasing them to the masses. JioSaavn Spotted will give undiscovered artists a chance to showcase their talent to JioSaavn’s users and music fans throughout India.</p>"},
        {title: 'In which languages will ‘JioSaavn Spotted’ be held?',
        copy: `
            <p>There are no language restrictions when it comes to JioSaavn Spotted because we intend this contest to be inclusive of all talent irrespective of language, genre, or geographical barriers. To makes things easier we have added the list of languages when it comes to the zonal rounds & the 3 standalone languages i.e. Hindi, Punjabi & English in the contest portals. If your language isn’t explicitly mentioned in the list that is already there, feel free to use the Other Eastern Languages, Other Western Languages, Other Southern Languages & Other Northern Languages tab when submitting your entry. Also, please be certain to specify the language of the entry clearly in the song description.</p>
            <p>Here’s the language calendar which is available on the Competitions page on our Spotted website. <a href="https://spotted.jiosaavn.com/competitions">https://spotted.jiosaavn.com/competitions</a></p>
            <p>For further clarification, the ZONAL ROUNDS will be broken down as follows:</p>
            <ul class="o-list">
                <li>East Zone In June 2022 - Participants may submit Assamese, Bengali, Odia, and/or any other Eastern language song.</li>
                <li>West Zone in August 2022 - Participants may submit Gujarati, Marathi, Rajasthani, and/or any other Western language song.</li>
                <li>South Zone in September 2022 - Participants may submit Kannada, Malayalam, Telugu, Tamil, and/or any other Southern language song.</li>
                <li>North Zone in November 2022 - Participants may submit Bhojpuri, Haryanvi, Himachali, Kashmiri, and/or any other Northern language song.</li>
            </ul>
        `},
        {title: 'What is the duration of the competition?',
        copy: "<p>JioSaavn Spotted 2022 will launch on 25th April and conclude on 10th December after the 4 Zonal Rounds [East, West, North, South] and 3 Language Rounds [English, Hindi, Punjabi]. Each of these rounds will happen during the specified month in the JioSaavn Spotted Calendar.</p>"},
        {title: 'How are the entries being judged? How will the winner be picked?',
        copy: `
            <p>Entries from each round will be selected on the basis of the eligibility criteria mentioned in the Contest Rules of JioSaavn Spotted.</p>
            <p>The shortlisted songs from each round will be shared in a playlist.</p>
            <p>The top 5 songs in each playlist with the greatest number of streams will be shared with a panel of celebrity judges who will then decide on the winner based on the originality, production, composition, recording, and musicianship of the Song.</p>
        `},
        {title: 'How will the participant be contacted upon being shortlisted or declared the winner?',
        copy: "<p>In both instances, someone from the JioSaavn Spotted team will reach out to you on your registered email address / contact number (The email ID given while submitting the entry).</p>"},
        {title: 'Who are the main judges for the competition?',
        copy: `
            <p>The permanent judges for JioSaavn Spotted 2022 are:</p>
            <ul class="o-list o-list-bare">
                <li>Salim Merchant</li>
                <li>DIVINE</li>
                <li>Jonita Gandhi</li>
                <li>Papon</li>
            </ul>
        `},
        {title: 'What do the winners get?',
        copy: "<p>The winner of each round gets a worldwide distribution deal with our label partner along with a fully-produced lyric video.</p>"},
    ],
    'Competition Timelines': [
        {title: 'How will Spotted be executed across each language?',
        copy: `
            <p>The contest flow for each month is as follows:</p>
            <ul class="o-list">
                <li>Call for entries for each round to go out on the 1st of every month during the Contest period and submissions will close on the 14th of that same month.</li>
                <li>After submissions close, JioSaavn will vet the entries based on the eligibility criteria set forth in the Contest Rules, and shortlist up to 15 songs from the pool of entries in each Zonal Round and Language Round.</li>
                <li>A Featured Playlist with all the shortlisted songs will be taken live on the 15th of that specific Zonal and Language round month, with JioSaavn IDs on the JioSaavn Platform.</li>
                <li>After observing the streaming data and the performance of all the songs in each Featured Playlist for a period of 15 days from when the songs were taken live on the JioSaavn platform, the top 5 songs with the greatest number of streams will be shared with our celebrity judges panel.</li>
                <li>They will then pick the winner from each Zonal and Language Round.</li>
                <li>The Winner gets a worldwide distribution deal with our label partner along with an accompanying lyric video.</li>
            </ul>
            <p>The JioSaavn Spotted 2022 Calendar:</p>
            <ul class="o-list">
                <li>Mid-April - Launch and Call for Entries from all regions and in all languages </li>
                <li>May - Hindi</li>
                <li>June - East Zone [Bengali, Assamese, Odia, and other Eastern language songs]</li>
                <li>July - Punjabi</li>
                <li>August - West Zone [Marathi, Gujarati, Rajasthani, and other Western language songs]</li>
                <li>September - South Zone [Telugu, Kannada, Malayalam, Tamil, and other Southern language songs]</li>
                <li>October - English</li>
                <li>November - North Zone [Haryanvi, Bhojpuri, Kashmiri, Himachali, and other North language songs]</li>
            </ul>
            `},
    ],
    'Competition Rules': [
        {title: "How do I participate in JioSaavn Spotted?",
        copy:`
        <p>You can participate and submit your entry in JioSaavn Spotted by visiting the JioSaavn Spotted website and following the exact steps mentioned on it.</p>
        <p>The procedure is as follows:</p>
        <ul class="o-list-bare">
            <li>Read the Contest Rules mentioned here thoroughly and make sure your entry meets all the eligibility criteria mentioned by us. If you aren’t able to meet the stipulated Contest Rules, your entry might be disqualified or not accepted.</li>
            <li>Fill up the submission form with all the required details.</li>
            <li>Wait for us to respond with updates.</li>
            <li>Be available for 2 weeks of promotions if you are shortlisted for the next round.</li>
        </ul>
        `},
        {title: "What kind of music can be submitted?",
        copy:"<p>There are no restrictions with respect to genres. All you need to note is that your entry will be considered for further rounds only when the respective Language Round or Zonal Round comes up in the Spotted calendar. </p>"},
        {title: "Is there any fee for joining the competition?",
        copy:"<p>There are no fees involved at any stage of JioSaavn Spotted.</p>"},
        {title: "Who can participate in the Spotted competition?",
        copy:"<p>Any Indian citizen over the age of 18 can participate in JioSaavn Spotted.</p>"},
        {title: "Is it fine if there are multiple collaborators on the entry song? E.g. - Someone is a composer, someone else sings and writes lyrics etc.",
        copy:"<p>Any artist who sings a song composed by another person i.e. a third party should ensure that he has the clear right and title to do so. At any stage during the contest or thereafter (if the song is chosen as a Winning Song) and if the music composer who is a third party raises a claim - JioSaavn or Warner will not be held responsible for this. In case of third party claim on the entry that it is an infringement on another person's lyrics or composition, the song will automatically be disqualified as an Entry. The same is also mentioned in page 7, point 2 (iv) of the Terms and Conditions.</p>"},
    ],
    'Participation Form': [
        {title: "How do I submit my entry for JioSaavn Spotted?",
        copy:`
            <p>All song submissions on the JioSaavn Spotted website will be accepted via FTP direct download links (e.g., Google Drive, Dropbox, WeTransfer).</p>
            <p>Only the song files that are shared in WAV or AAC format will be accepted and no other audio formats will be accepted as eligible entries. For the avoidance of any doubt, formats like MP3, MP4, M4A, FLAC, WMA will not be accepted as an Eligible Entry.</p>
        `},
        {title: "How to share links of my image & song in the participation form?",
        copy:`
            <p>The best way to upload both is by putting them in one Google Drive folder (or similar storage drive) and sharing them with an open access (right click on the folder and select "Share", then select ‘Anyone with link’ & 'Editor' in access). Copy the link of the folder(s) & upload the link without any edits in the specified tabs on the submissions portal.</p>
            <p>You can also choose to use other file sharing platforms like WeTransfer or Dropbox but ensure that the uploaded links are not set to expire after a set period, and have the editor access provided for downloading and listening.</p>
        `},
        {title: "Can I submit a multi-lingual song as an entry?",
        copy:"<p>Yes, but the song will be judged on the basis of the primary language specified at the time of submission. If the language of the song is deemed to be inaccurate from the submission form, JioSaavn may at its’ sole discretion decide upon the eligibility of the song.</p>"},
        {title: "Can I use YouTube or SoundCloud links for submission?",
        copy:"<p>No, SoundCloud or YouTube links will not be accepted for submitting entries.</p>"},
        {title: "Can I submit multiple entries?",
        copy:"<p>Yes, but you can only submit one song per Zonal Round and Language Round.</p>"},
        {title: "Can I submit multiple entries for one language?",
        copy:"<p>No, you cannot. If an artist submits multiple entries for 1 Zonal Round or Language Round, only the first submission made will be considered for JioSaavn Spotted.</p>"},
        {title: "Can I submit a song even if it is live on YouTube or any other social media/music platform?",
        copy:"<p>No, your submission needs to be an original & unreleased song that cannot be live on any other platform prior to the commencement of the Contest and during.</p>"},
        {title: "What kind of artist story or journey should I share?",
        copy:"<p>Every artist’s journey is unique and special. The best way to showcase yourself is to highlight how authentic you are to the music you are putting out. Mention how you see yourself as an artist, the people you’ve collaborated with in the past. Tell us where you are currently in your journey, and also where you are coming from because we sincerely want to know the myriad of different factors that helped with shaping your music. In short, tell us why you make music.</p>"},
        {title: "Should I share the story or the creative process behind the track?",
        copy:"<p>Yes, the more information you can give us regarding your entry, the better we will understand you as an artist.</p>"},
        {title: "Will I be granting JioSaavn rights to my song when I participate in Spotted?",
        copy:"<p>You will not grant JioSaavn any ownership interest in the song you share, however you will grant JioSaavn an irrevocable, assignable, non-exclusive licence in the copyright of the Entry for the purposes of streaming the song on the JioSaavn platform until the end of the term as defined in the Contest Rules.</p>"},
        {title: "Upon winning, will the label re-master my original track? Do I have to submit another track upon winning?",
        copy:"<p>The winning entry will be remastered/remixed on the basis of discussion between the Winner and the Label, for a new global release within a few months of it being declared the winner.</p>"},
    ],
    'Song Shortlisting': [
        {title: "Where can one stream all the top tracks?",
        copy:"<p>The shortlisted entries will be available on JioSaavn for streaming from the 15th of that respective month for that language/zonal round.</p>"},
        {title: "Can I share my track with friends, family, or fans?",
        copy:"<p>Yes, once it’s live on the JioSaavn Platform, you can share it with your friends, and fans. Remember, the higher the number of streams, the better your chances of being that round’s winner.</p>"},
        {title: "How are streams counted for the Tracks?",
        copy:"<p>The Track gets one stream every time a user listens to it for 30 seconds or more on JioSaavn.</p>"},
        {title: "For what duration will the stream counts be considered for the shortlisted entries in each language/round of the contest?",
        copy:"<p>Streams for the contest will be considered and tabulated from the 15th of the month of the respective Zonal and Language Rounds to the 30th of that same month.</p>"},
    ],
    'Label Deal': [
        {title: "How many winning artists will be signed on by a recording label?",
        copy:"<p>7 winners will be signed by our label partner; one from each round of JioSaavn Spotted (4 Zonal rounds + 3 Language rounds).</p."},
        {title: "What kind of deal/agreement will the artist and label get into?",
        copy:"<p>The agreement will be based on discussions entirely between the label and the artist. JioSaavn will facilitate the assignment of the Winning Song to the label and re-release the remastered version on the JioSaavn Platform globally.</p>"},
        {title: "What else do the winning artists get apart from the label deal/agreement?",
        copy:"<p>Artists get the golden opportunity to showcase their music to millions of users across India.</p>"},
    ]
}