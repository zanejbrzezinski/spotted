/**
 * Imports
 */
import { PAGE_MAPPINGS } from './constants';

/**
 * Sidebar
 */
const Sidebar = (function() {

    /**
     * Variables
     */
    const button = document.querySelectorAll('.sidebar-btn');
    const sidebar = document.querySelector('#sidebar');

    /**
     * toggle sidebar
     */
    const toggleSidebar = function() {
        sidebar.classList.contains('open') ? sidebar.classList.remove('open') : sidebar.classList.add('open');
        window.gtag('event', 'click', {
            'screen_name': 'spotted',
            'screen_page_id': PAGE_MAPPINGS[window.location.pathname],
            'entity_name': 'sidebar_button',
            'entity_type': 'button',
            'entity_pos': 'header'
        })
    };

    /**
     * Initialize
     */
    const init = function() {
        button.forEach(el => el.addEventListener('click', toggleSidebar))
    };


    /**
     * Public methods
     */
    return { init };

})();





/**
 * Exports
 */
export default Sidebar;




