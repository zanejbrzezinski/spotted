
/**
 * Sidebar
 */
const Video = (function() {

    /**
     * Variables
     */
    const video = document.querySelector('#video');
    const body = document.querySelector('body');
    const about = document.querySelector('#btn-fade-in');
    const logo = document.querySelector('#logo');
    const wrapper = document.querySelector('.c-video');
    const loader = document.querySelector('.c-video .c-loader')
    const close = document.querySelector('.c-video__close');
    const volume = document.querySelector('.c-video__volume');
    const seekBar = document.querySelector('.c-video__seek')
    const progress = document.querySelector('.c-video__progress');
    let muted = true;


    /**
     * Toggle Mute
     */
    const toggleMute = function() {
        if (muted) {
            volume.classList.remove('mute');
        } else {
            volume.classList.add('mute');            
        }

        muted = !muted;
        video.muted = !video.muted;
    };

    /**
     * Move progress bar on time play
     */
    const timeUpdate = function(){
        const percent = (video.currentTime / video.duration) * 100;
        progress.style = `width: ${percent}%`
    }

    const pauseOnScroll = function() {
        const { top, bottom, height } = video.getBoundingClientRect()
        const isVisible =  top < window.innerHeight && bottom >= 0;
        if (!isVisible) { video.pause(); }
    }


    /**
     * Seek
     */
     const seek = function(e){
        const left = (e.clientX - seekBar.getBoundingClientRect().left);
        const totalWidth = seekBar.offsetWidth;
        const percentage = ( left / totalWidth );
        const vidTime = video.duration * percentage;
        video.currentTime = vidTime;
    }

    /**
     * hide video
     */
     const hideVideo = function(e){
        video.pause();
        wrapper.classList.add('fade');
        about.style = "transition:  opacity .3s ease-in-out; opacity: 1";
        logo.style = "transition:  opacity .3s ease-in-out; opacity: 1";
    }


    /**
     * Add Events
     */
    const addEvents = function() {
        volume.addEventListener('click', toggleMute);
        seekBar.addEventListener('click', seek);
        video.addEventListener('click', () => video.paused ? video.play() : video.pause())
        video.ontimeupdate = timeUpdate;
        close.addEventListener('click', hideVideo);
        video.addEventListener('canplay', () => loader.classList.add('u-hidden'));
        window.addEventListener('scroll', pauseOnScroll)
    };

    /**
     * Initialize
     */
    const init = function() {
        addEvents();
    };


    /**
     * Public methods
     */
    return { init };

})();





/**
 * Exports
 */
export default Video;




