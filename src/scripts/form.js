/**
 * Imports
 */

import { forEach as _forEach, map as _map } from 'lodash';
import axios from 'axios';
import Toast from './toast'






/**
 * Form module
 */
const Form = (function (props) {

    /**
     * Private properties
     */
    const container = document.getElementsByName(props.name)[0];
    const inputs = container.querySelectorAll('input, textarea, select');
    const button = container.querySelector('button[type="submit"]');
    const errors = [];
    const postData = new FormData();
    let params = props.data;
    let active = '';
    let isDirty = false;

    const TAB = 9;
    const SHIFT = 16;

    let config = {
        headers: {
            'Access-Control-Allow-Headers': '*',
        }
    }



    /**
     * Add error message
     */
    const _addError = function(input, error) {
        const parent = input.parentElement.parentElement;
        const { name } = input;

        errors[name] = error.id;

        const errorDisplay = parent.querySelector('.error-message');
        if (errorDisplay ) {
            errorDisplay.textContent = error.message;
        }
        parent.classList.add('field-error');
    

        return error;
    };


    /**
     * REmove error message
     */
    const _removeError = function(input, id) {
        const parent = input.parentElement.parentElement;
        const { name } = input;

        parent.classList.remove('field-error');
        errors[name] = null;
    };


    /**
     * Input validation
     */
    const _checkErrors = function(input) {
        const parent = input.parentElement;
        const value = input.value;
        const required = input.getAttribute('data-required') !== 'false';
        let { min, name, type } = input;
        let re, error = { id: '', message: '' };

        // Check for required inputs
        if (required) {
            error.id = `${name}_required`;
            error.message = `${name} field is required`;
            if (value === '' || (type === 'checkbox' && !input.checked) ) {
                if (errors[name] === error.id) return error;
                if (errors[name] && errors[name] !== error.id) _removeError(input, errors[name]);

                return _addError(input, error);
            } else {
                _removeError(input, error.id);
            }
        }

        // Legacy browser check
        type = !Modernizr.inputtypes.email && /email/i.test(name) ? 'email' : type;

        // Error checking based on type
        switch (type) {
            case 'email' :
                error.id = `${name}_email`;
                error.message = 'Must be a valid email address';
                re = /^\w[a-zA-Z0-9\.\-\_]+[^@\.\_\-]@[^@\.][a-zA-Z\.\-\_]+\.[a-zA-Z]{2,}$/ig;

                if (!re.test(value)) {
                    if (errors[name] || errors[name] === error.id) return error;

                    return _addError(input, error);
                } else {
                    _removeError(input, error.id);
                }

                break;
            case 'url' : 
                if (!required && value === '' ) return;
                error.id = `${name}_url`;
                error.message = 'Must be a valid url';
                re = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
                if (!re.test(value)) {
                    if (errors[name] || errors[name] === error.id) return error;

                    return _addError(input, error);
                } else {
                    _removeError(input, error.id);
                }
            default:
                error.id = `${name}_min_invalid`;
                error.message = `Must have at least ${min} characters.`;

                if (min && value.length < min) {
                    if (errors[name] || errors[name] === error.id) return error;

                    return _addError(input, error);
                } else {
                    _removeError(input, error.id);
                }

                break;
        }

        // If valid, set form data
        postData.append(name, value);
        return;
    };

    const _clearForm = function(){
        _forEach(inputs, (input) => { input.value = '' });
        isDirty = false;
    }


    /**
     * Handle form submission
     */
    const _submit = function(e) {
        console.log("SUBMIT");
        e.preventDefault();

        window.gtag('event', 'click', {
            'screen_name': 'spotted',
            'screen_page_id': 'participate',
            'entity_name': 'submit',
            'entity_type': 'button',
        })

        // Disallow multiple submission
        button.setAttribute('disabled', 'disabled');
        button.classList.add('c-btn--loading');

        // Validation
        let formErrors = _map(inputs, (input) => _checkErrors(input));
        formErrors = formErrors.filter((err) => err);
        console.log('FORM ERRORS', formErrors);
        // Check if errors exist
        if (formErrors.length) {
            button.removeAttribute('disabled');
            button.classList.remove('c-btn--loading');
            return container.querySelector('.field-error input').focus();
        }

        // Set params for API call
        _forEach(inputs, (input) => { params[input.name] = input.value; });

        // Set security form data
        postData.append('action', 'submit_form_process');
        postData.append('security', grecaptcha.getResponse());

        /* Submit the music */
        axios.post(params.ajaxUrl, postData)
        .then((response) => {
            const data = response.data;
            if (data.status === 'success') {
                grecaptcha.reset();
                button.removeAttribute('disabled');
                _clearForm();
                Toast.add({ id: `${props.name}_success`, text: 'Form Submitted', timer: 5000 });
                gtag('event', 'success', {
                    'context': 'success_participation',
                })
            } else {
                grecaptcha.reset();
                button.removeAttribute('disabled');
                Toast.add({ id: `${props.name}_error`, text: data.message, error: true, timer: 5000 });
                gtag('event', 'failure', {
                    'context': 'failure_participation',
                    'error_msg': data.message
                })
            }
        })
    };


    /**
     * Keydown event
     */
    const _keyDown = (e) => {
        if (e.target.name === active && e.keyCode !== TAB && e.keyCode !== SHIFT) {
            isDirty = true;
        }
    };


    /**
     * Blur event
     */
    const _blur = (e) => {
        if (isDirty || e.target.value !== '') {
            _checkErrors(e.target);
        }
    };


    /**
     * Bind events
     */
    const _bindEvents = function() {
        /* Handle form submission */
        button.addEventListener('click', _submit);

        /* Input actions */
        _forEach(inputs, (input) => {
            errors[input.name] = null;

            input.addEventListener('focus', (e) => { active = e.target.name });
            input.addEventListener('keydown', _keyDown);
            input.addEventListener('blur', _blur);
        });

        window.addEventListener('beforeunload', function(){
            if (isDirty) {
                var object = {};
                postData.forEach(function(value, key){
                    object[key] = value;
                });
                var json = JSON.stringify(object);

                gtag('event', 'window_close', {
                    'screen_name': 'spotted',
                    'screen_page_id': 'participate',
                    'entity_name': 'form',
                    'entity_type': 'form',
                    'extraInfo': json
                })
            }

        })
    };


    /**
     * Initialize module
     */
    const init = function() {
        _bindEvents();
    };


    /**
     * Public methods
     */
    return { init };

});





/**
 * Exports
 */
export default Form;




