/**
 * Imports
 */
import { PAGE_MAPPINGS } from './constants';

/**
 * Carousel
 */
const Carousel = function (isLarge) {

    /**
     * Variables
     */
    let ctr, left, right, inner, count, isMobile;
    let shift = 0;

    /**
     * Shift Carousel
     */
    const shiftCarousel = function(move) {
        isMobile = window.innerWidth <= 980 ? true : false;

        let limit = isMobile || isLarge ? 0 - count : 0 - (count / 3);

        if (shift + move <= 0 && shift + move > limit) {
            left.classList.remove('hide');
            right.classList.remove('hide');

            shift += move;

            inner.style.transform = `translateX(${100 * shift}%)`
        }

        if ( shift + move > 0) {
            left.classList.add('hide');
        }

        if ( shift + move <= limit ) {
            right.classList.add('hide');
        }
    };

    /**
     * Initialize
     */
    const init = function(props) {
        ctr = document.querySelector("#" + props.id);
        left = ctr.querySelector('.c-carousel__left');
        right = ctr.querySelector('.c-carousel__right');
        inner = ctr.querySelector('.c-carousel__inner');
        left.addEventListener('click', () => {
            shiftCarousel(1);
            window.gtag('event', 'click', {
                'screen_name': 'spotted',
                'screen_page_id': PAGE_MAPPINGS[window.location.pathname],
                'entity_name': 'carousel_left',
                'entity_type': 'link',
                'entity_pos': props.id
            })
        });
        right.addEventListener('click', () => {
            shiftCarousel(-1);
            window.gtag('event', 'click', {
                'screen_name': 'spotted',
                'screen_page_id': PAGE_MAPPINGS[window.location.pathname],
                'entity_name': 'carousel_right',
                'entity_type': 'link',
                'entity_pos': props.id
            })
        });
        count = ctr.querySelectorAll('.c-carousel__item').length;
        if (count < 2) {
            left.classList.add('u-hidden');
            right.classList.add('u-hidden');
        }

        // reset carousel on window resize.
        window.addEventListener('resize', () => {
            isMobile = window.innerWidth <= 980;
            shiftCarousel(1);
        })
    };


    /**
     * Public methods
     */
    return { init };

};





/**
 * Exports
 */
export default Carousel;




