/**
 * Imports
 */
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';




/**
 * Rotator component
 */
const Rotator = () => {

    /**
     * Properties
     */
    const [ status, setStatus ] = useState('LOADING');

    /**
     * On component load
     */
    useEffect(() => {
        setTimeout(() => setStatus('SUCCESS'), 1000);
    }, []);

    /**
     * View rendering
     */
    if (status === 'LOADING') return 'Loading....';
    return 'ROTATOR';

};





/**
 * Default export
 * 
 * @param {string} id
 * @param {object} props
 */
export default function({ id, props }) {
    const el = document.getElementById(id);

    if (!el) return console.error('Required fields are missing.');

    return ReactDOM.render(React.createElement(Rotator, props, null), el);
}




