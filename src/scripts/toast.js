/**
 * Toast module
 */
const Toast = (function () {

    /**
     * Remove a toast message
     */
    const remove = function(props) {
        const container = document.querySelector('.c-toast');
        const message = container.querySelector(`[id="${props.id}"]`);

        if (message) {
            message.classList.add('c-toast__message--remove');
            return setTimeout(() => container.removeChild(message), 600);
        }

        return;
    };


    /**
     * Add a toast message
     */
    const add = function(props) {
        const container = document.querySelector('.c-toast');
        const message = document.createElement('div');
        const lowerText = props.text && props.text.replace(/[-_]/g, ' ');

        if (container.querySelector(`[id="${props.id}"]`) !== null) return;

        message.id = props.id;
        message.className = props.error ? 'c-toast__message c-toast__message--error' : 'c-toast__message';
        message.innerText = lowerText ? lowerText.replace(/^\w/, c => c.toUpperCase()) : props.text;
        container.appendChild(message);

        if (props.timer && props.timer > 1000) {
            setTimeout(() => remove({ id: props.id }), props.timer);
        }

       return setTimeout(() => message.classList.add('c-toast__message--active'));
    };


    /**
     * Public methods
     */
    return { add, remove };

})();





/**
 * Exports
 */
export default Toast;




