/**
 * Imports
 */
import { PAGE_MAPPINGS } from './constants';

/**
 * Gtag
 */
const GTag = (function() {

    const sidebar = document.querySelector('#sidebar');
    const header = document.querySelector('.c-header');
    const footer = document.querySelector('.c-footer__nav');

    /**
     * Return type of icon
     */
    const getIconDestination = function(link){
        if (link.querySelector('.o-icon-facebook')) { return 'facebook'}
        if (link.querySelector('.o-icon-twitter')) { return 'twitter'}
        if (link.querySelector('.o-icon-instagram')) { return 'instagram'}
        if (link.querySelector('svg')) { return 'logo'}
    }


    /**
     * Create click events for links on all pages (Sidebar, Header, Footer)
     */
    const createEventConstants = function(){
        sidebar.querySelectorAll('a').forEach((link) => {
            link.addEventListener('click', function(e){
                window.gtag('event', 'click', {
                    'screen_name': 'spotted',
                    'screen_page_id': PAGE_MAPPINGS[window.location.pathname],
                    'entity_name': link.innerText.length ? link.innerText : getIconDestination(link),
                    'entity_type': 'link',
                    'entity_pos': 'sidebar'
                })
            })
        })
        header.querySelectorAll('a').forEach((link) => {
            link.addEventListener('click', function(e){
                window.gtag('event', 'click', {
                    'screen_name': 'spotted',
                    'screen_page_id': PAGE_MAPPINGS[window.location.pathname],
                    'entity_name': link.innerText.length ? link.innerText : getIconDestination(link),
                    'entity_type': 'link',
                    'entity_pos': 'header'
                })
            })
        })
        footer.querySelectorAll('a').forEach((link) => {
            link.addEventListener('click', function(e){
                window.gtag('event', 'click', {
                    'screen_name': 'spotted',
                    'screen_page_id': pageMappings[window.location.pathname],
                    'entity_name': link.innerText.length ? link.innerText : getIconDestination(link),
                    'entity_type': 'link',
                    'entity_pos': 'footer'
                })
            })
        })
    }

    /**
     * Initialize
     */
    const init = function() {

        //Page View
        window.gtag('event', 'view', {
            'screen_name': 'spotted',
            'screen_page_id': PAGE_MAPPINGS[window.location.pathname]
        })

        createEventConstants();

    };


    /**
     * Public methods
     */
    return { init };

})();





/**
 * Exports
 */
export default GTag;




