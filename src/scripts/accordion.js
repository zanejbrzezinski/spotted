import { FAQS } from './constants';


/**
 * Accordion
 */
const Accordion = (function() {

    /**
     * Variables
     */
    const accordionsCTR = document.querySelector('#accordions');
    const accordionCopy = document.querySelector('#accordion-copy');
    const accordionList = document.querySelector('#accordion-list');
    const headerCopy = document.querySelector('#header-copy');
    const catsCTR = document.querySelector('.c-accordion__cats');
    const catDisplay = document.querySelector('#cat-display');
    const expand = document.querySelectorAll('.expand-all');
    const expandText = document.querySelectorAll('.expand-all *');
    const select = document.querySelector('#category-select');
    let category = Object.keys(FAQS)[0].split(' ').join('_');
    let shouldExpand = true;

    /**
     * Check for dom parser support
     */
    var support = (function () {
        if (!window.DOMParser) return false;
        var parser = new DOMParser();
        try {
            parser.parseFromString('x', 'text/html');
        } catch(err) {
            return false;
        }
        return true;
    })();

    /**
     * Convert Text to HTML
     */
    var textToHTML = function (str) {

        // check for DOMParser support
        if (support) {
            var parser = new DOMParser();
            var doc = parser.parseFromString(str, 'text/html');
            return doc.body.innerHTML;
        }

        // Otherwise, create div and append HTML
        var dom = document.createElement('div');
        dom.innerHTML = str;
        return dom;

    };

    const setExpandText = function(){
        expandText.forEach((el) => {
            shouldExpand ? el.innerText = 'Expand All' : el.innerText = 'Collapse All';
        })
    }



    /**
     * Toggle Accordion
     */
    const toggleAccordion = function(el) {
        this.classList.contains('open') ? this.classList.remove('open') : this.classList.add('open');
        const btn = this.querySelector('.c-accordion__btn')
        if (btn.classList.contains('open')){
            shouldExpand = false;
            setExpandText();
            btn.classList.remove('open')
            btn.classList.add('close')
        } else {
            btn.classList.add('open')
            btn.classList.remove('close')
        }
    }

    /**
     * Add Event Listeners
     */
     const setCategory = function(){
        if (this.id !== 'category-select') {
            category = this.id;            
        } else {
            category = this.value;
        }
        catDisplay.innerText = this.id.split('_').join(' ');
        shouldExpand = true;
        expandText.innerText = "Expand All"

        catsCTR.querySelectorAll('li').forEach((li) => {
            if (li.id === category) {
                li.classList.add('active');
            } else {
                li.classList.remove('active');
            }
        })

        accordionsCTR.querySelectorAll(':scope > ul').forEach((ul) => {  
            if (ul.id === category) {
                ul.classList.remove('u-hidden');
            } else {
                ul.classList.add('u-hidden');
            }
        })
    }

    /**
     * Expand All
     */
    const expandAll = function() {
        if (shouldExpand) {
            shouldExpand = false;
            accordionsCTR.querySelectorAll(':scope > ul > li').forEach((li) => {
               li.classList.add('open');
               li.querySelector('.c-accordion__btn').classList.remove('open')
               li.querySelector('.c-accordion__btn').classList.add('close')
            })
        } else {
            shouldExpand = true;
            accordionsCTR.querySelectorAll(':scope > ul > li').forEach((li) => {
               li.classList.remove('open');
               li.querySelector('.c-accordion__btn').classList.add('open')
               li.querySelector('.c-accordion__btn').classList.remove('close')
            })
        }
        setExpandText();
    }


    /**
     * Add Event Listeners
     */
    const addEvents = function() {
        const accordions = accordionsCTR.querySelectorAll('.c-accordion');
        accordions.forEach(el => el.addEventListener('click', toggleAccordion));

        catsCTR.querySelectorAll('li').forEach((li) => {
            li.addEventListener('click', setCategory)
        })

        expand.forEach((el) => {
            el.addEventListener('click', expandAll);
        })

        select.addEventListener('change', setCategory)
    };

    /**
     * Create Accordion Markup
     */
    const createMarkup = function() {

        Object.keys(FAQS).map((key) => {
            //create header
            let elem = headerCopy.cloneNode(true);
            elem.id = key.split(' ').join('_');
            elem.classList.remove('u-hidden');
            elem.querySelector('h4').innerText = key;
            catsCTR.append(elem);

            let opt = document.createElement('option');
            opt.value = key.split(' ').join('_');
            opt.innerText = key;

            //create Accordions
            let list = accordionList.cloneNode();
            if (key.split(' ').join('_') === category) { 
                list.classList.remove('u-hidden')
                elem.classList.add('active');
                catDisplay.innerText = key;
                opt.selected = true;
            };
            list.id = key.split(' ').join('_');
            select.append(opt);

            FAQS[key].map((item) => {
                let acc = accordionCopy.cloneNode(true);
                acc.classList.remove('u-hidden');
                acc.querySelector('h4').innerText = item.title;
                acc.querySelector('.c-accordion__inner div').innerHTML = textToHTML(item.copy);
                list.append(acc);
            })

            accordionsCTR.append(list)
        });

        addEvents();

    }

    /**
     * Initialize
     */
    const init = function() {
        createMarkup();
        // addEvents();
    };


    /**
     * Public methods
     */
    return { init };

})();





/**
 * Exports
 */
export default Accordion;




