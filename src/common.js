/*==============================
=            STYLES            =
==============================*/

// Run through here for processing,
// but extracted to a new file

import './scss/styles.scss';

/*=====  END OF STYLES  ======*/





/*===============================
=            SCRIPTS            =
===============================*/

import('./scripts/sidebar.js').then(module => { module.default.init(); });
import('./scripts/gtag.js').then(module => { module.default.init(); });

/*=====  END OF SCRIPTS  ======*/




