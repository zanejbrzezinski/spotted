/*===============================
=            SCRIPTS            =
===============================*/

// import('./scripts/form.js').then(module => { module.default.init({ name: 'participate_now' }); });

import { default as Form } from './scripts/form.js';

const ajaxUrl = process.env.NODE_ENV === 'production' ? 'https://www.jiosaavn.com/api.php?__call=spotted.participateForm&name=daa&alias=dsfa[…]aa&description=dsadas&api_version=4&_format=json&_marker=0': 'https://staging.jiosaavn.com/api.php?__call=spotted.participateForm&name=daa&alias=dsfa[…]aa&description=dsadas&api_version=4&_format=json&_marker=0';

// const ajaxUrl = 'https://staging.jiosaavn.com/api.php?__call=spotted.participateForm&name=daa&alias=dsfa[…]aa&description=dsadas&api_version=4&_format=json&_marker=0';

new Form({ name: 'participate_now', data: { ajaxUrl: ajaxUrl } }).init();



/*=====  END OF SCRIPTS  ======*/




