/* ==========================================================================
   #TILES
   ========================================================================== */

.c-tile {
    padding: $inuit-global-spacing-unit*3 $inuit-global-spacing-unit;
    text-align: center;
    border-radius: 25px;
    position: relative;
    overflow: hidden;
    box-shadow: -5px 8px 8px 0 rgba(0,0,0,.4);
}


    .c-tile__header,
    .c-tile__sub {
        color: color(s-white);
        font-style: italic;
        z-index: 1;
        position: relative;
    }

    .c-tile__header {
        font-size: 38px;
        font-style: italic;
    }

    .c-tile__sub {
        font-weight: 100;
        font-size: 25px;
        margin-bottom: $inuit-global-spacing-unit;
    }

    .c-tile__btn {
        font-size: 16px;
        letter-spacing: normal;
        z-index: 1;
        position: relative;
    }

    .c-tile__pill {
        position: absolute;
        top: 10px;
        left: 10px;
        background: color(s-yellow-alt);
        border-radius: 9999px;
        font-size: 15px;
        line-height: 30px;
        padding: 0 13px;
        font-weight: 300;
        font-style: italic;
        text-transform: capitalize;
        z-index: 1;
    }

    .c-tile__bg {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 0;
    }

/* Variations
   ========================================================================== */

.c-tile--large {
    padding: $inuit-global-spacing-unit*3 0;
    .c-tile--header {
        font-size: 38px;
    }

    .c-tile__bg {
        left: 75%;
    }
}

.c-tile--0 {
    background: linear-gradient(0turn, #80070b, #cc292f);
}

.c-tile--1 {
    background: #1a5b52;
}

.c-tile--2 {
    background: linear-gradient(.25turn, #650c65, #420a4d);
}

.c-tile--3 {
    background: linear-gradient(.125turn, #003556, #002c4a);
}

.c-tile--4 {
    background: linear-gradient(.25turn, #002750, #002944);
}

.c-tile--5 {
    background: linear-gradient(.25turn, #3e2300, #573100);
}

.c-tile--6 {
    background: #012675;
}


/* Responsive
   ========================================================================== */


@include mq($from: lg) {

    .c-tile--large {
        padding: $inuit-global-spacing-unit*3;

        .c-tile__header {
            font-size: 80px;
        }
    }

    .c-tile__header {
        margin-bottom: $inuit-global-spacing-unit-tiny;
        line-height: 1;
        font-weight: 700;
    }

    .c-tile__sub {
        margin-bottom: 0;
    }

    .c-tile__btn {
        font-size: 20px;
    }

}